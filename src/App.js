import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Page/Home';
import Register from './Componen/Register';
import Login from './Componen/Login';

function App() {
  return (
    <div >
     <BrowserRouter>
     <Switch>
     <Route path="/home" component={Home} exact />
     <Route path="/register" component={Register} exact />
     <Route path="/" component={Login} exact />
     </Switch>
     </BrowserRouter>
    </div>
  );
}

export default App;
